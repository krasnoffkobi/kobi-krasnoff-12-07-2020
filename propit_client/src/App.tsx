import React, { Component } from 'react';
import './App.scss';
import HeaderComponent from './Header/header';
import { getDataSaga } from './actions/rest';
import { RSS_URL, DEVICE_LIST_LODED } from './actions/types';
import { connect } from 'react-redux';

type  SampleProps = {
  getDataSaga: any
}

class App extends Component<SampleProps> {
  constructor(props: SampleProps) {
    super(props);
  }
  
  state = {
    OriginalXMLResponse: {},
    TrackPlayerList: []
  }

  args = {
    str: 'sdfsdfsdf',
    baseURL: RSS_URL,
    callbackFunction: DEVICE_LIST_LODED
  }

  componentDidMount() {
    // activate   
    this.props.getDataSaga(this.args);  
    
  };
  
  render() {
    return (
      <div>
        <HeaderComponent></HeaderComponent>
        <div>
          table
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state: any) => {
  return {
    OriginalXMLResponse: state.OriginalXMLResponse,
    TrackPlayerList: state.TrackPlayerList,
    errorCode: state.errorCode
  }
}

const mapDispatchToProps = (dispatch: any) => {
  return {
    getDataSaga: (args: any) => dispatch(getDataSaga(args))
  }
}

// export default App;
export default connect(mapStateToProps, mapDispatchToProps)(App);
