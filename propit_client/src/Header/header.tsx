import React, { Component } from "react";
import PropiTLogoblack from '../images/PropiTLogoblack.png';
import e from '../images/e.png';
import './header.scss';

class HeaderComponent extends Component {
    // A react-chart hyper-responsively and continuusly fills the available
    // space of its parent element automatically
      render() {
          return (
            <div className="App">
              <header className="App-header">
              <div className="leftSide">
                <div className="Logo">
                  <img src={PropiTLogoblack} />
                </div>
                <div className="phone">
                  <a href="mailTo:info@propt.com"><img src={e} /></a>
                  <div className="text"><a href="tel:077-9985041">077-9985041</a></div>
                </div>
              </div>
              <div className="rightSide">
                <div className="item"><a href="#">מועדפים</a></div>
                <div className="item"><a href="#">מחשבון שטחים</a></div>
                <div className="item"><a href="#">הוספת נכס</a></div>
                <div className="item"><a href="#">תגמול שותפים</a></div>
                <div className="item"><a href="#">קבל הצעות אישיות</a></div>
                <div className="item"><a href="#">לידים חמים</a></div>
              </div>
              </header>
            </div>
          );
      }
    
      
    
  };
  
  export default HeaderComponent;