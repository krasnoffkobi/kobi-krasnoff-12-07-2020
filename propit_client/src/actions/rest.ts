import { DATA_REQUESTED } from './types';

export function getDataSaga(args: any) {
    return { type: DATA_REQUESTED, args }; 
}