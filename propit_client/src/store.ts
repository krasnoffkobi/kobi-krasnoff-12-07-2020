import { createStore, applyMiddleware, compose } from "redux";
import rootReducer from "./reducers/restReducers";
import createSagaMiddleware from "redux-saga";
import apiSaga from "./sagas/api-saga";

declare global {
    interface Window {
      __REDUX_DEVTOOLS_EXTENSION_COMPOSE__?: typeof compose;
    }
};

const storeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const initialiseSagaMiddleware = createSagaMiddleware();

const store = createStore(
    rootReducer, 
    storeEnhancers(applyMiddleware(initialiseSagaMiddleware)));

initialiseSagaMiddleware.run(apiSaga);

export default store;